#!/usr/bin/python3

import time

import serial
import argparse

from oscpy.client import OSCClient
from oscpy.server import OSCThreadServer

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--serial", help="serial port")

args = parser.parse_args()

print(args.serial)

serial_port = "/dev/ttyACM1"

if args.serial:
    serial_port = args.serial

osc_client = OSCClient("0.0.0.0", 9876)
osc_server = OSCThreadServer()

osc_server.listen("0.0.0.0", port=6789, default=True)

s = serial.Serial(serial_port, 115200)


@osc_server.address(b'/trellis')
def trellis(x, y, val):
    print(x, y, val)
    msg = "s %s %s %s\n" % (val, x, y)
    s.write(msg.encode())
    return "ok"


@osc_server.address(b'/trellis/c')
def trellis_c(x, y, val):
    print(x, y, val)
    val = val.decode("utf-8")
    msg = "c %s %s %s\n" % (val, x, y)
    s.write(msg.encode())
    return "ok"


@osc_server.address(b'/trellis/list')
def trellis_list(val):
    print(len(val), val)
    val = val.decode("utf-8")
    now = time.time()
    for i in range(8):
        msg = "r %s %s\n" % (i, val[i*16:(i+1)*16])
        s.write(msg.encode())
        print(time.time() - now)
        now = time.time()
    return "ok"


@osc_server.address(b'/trellis/off')
def trellis_off():
    print("off")
    msg = "off\n"
    s.write(msg.encode())
    return "ok"


print("start")
now = time.time()
while 1:
    msg = s.read_until()
    msg = msg.decode("utf-8")
    msg = msg.split()
    if len(msg) == 3:
        osc_client.send_message(b"/trellis", [int(msg[1]), int(msg[2]), int(msg[0])])

    print("%.4f" % (time.time() - now), " ".join(msg))
    now = time.time()



