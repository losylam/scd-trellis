/* This example shows basic usage of the
MultiTrellis object controlling an array of
NeoTrellis boards

As is this example shows use of two NeoTrellis boards
connected together with the leftmost board having the
default I2C address of 0x2E, and the rightmost board
having the address of 0x2F (the A0 jumper is soldered)
*/

#include <SerialCommand.h>

#include "Adafruit_NeoTrellis.h"

#define Y_DIM 8 //number of rows of key
#define X_DIM 16 //number of columns of keys

SerialCommand sCmd;

//create a matrix of trellis panels
Adafruit_NeoTrellis t_array[Y_DIM/4][X_DIM/4] = {
  { Adafruit_NeoTrellis(0x39), Adafruit_NeoTrellis(0x35), Adafruit_NeoTrellis(0x32), Adafruit_NeoTrellis(0x33)},
  { Adafruit_NeoTrellis(0x2E), Adafruit_NeoTrellis(0x2F), Adafruit_NeoTrellis(0x30), Adafruit_NeoTrellis(0x31)}
};

/*
If you were using a 2x2 array of NeoTrellis boards, the above lines would be:

#define Y_DIM 8 //number of rows of key
#define X_DIM 8 //number of columns of keys

//create a matrix of trellis panels
Adafruit_NeoTrellis t_array[Y_DIM/4][X_DIM/4] = {
  { Adafruit_NeoTrellis(0x2E), Adafruit_NeoTrellis(0x2F) },

  { Adafruit_NeoTrellis(LOWER_LEFT_I2C_ADDR), Adafruit_NeoTrellis(LOWER_RIGHT_I2C_ADDR) }
};
*/


//pass this matrix to the multitrellis object
Adafruit_MultiTrellis trellis((Adafruit_NeoTrellis *)t_array, Y_DIM/4, X_DIM/4);

// Input a value 0 to 255 to get a color value.
// The colors are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  int r, g, b;

  if(WheelPos < 85) {
    r = WheelPos * 3;
    g = 255 - WheelPos * 3;
    b = 0;
  } else if(WheelPos < 170) {
    WheelPos -= 85;
    r = 255 - WheelPos *3;
    g = 0;
    b = WheelPos*3;
  } else {
    WheelPos -= 170;
    r = 0;
    g = WheelPos * 3;
    b = 255 - WheelPos*3;
  }
  return seesaw_NeoPixel::Color(r/3, g/3, b/3);
}

//define a callback for key presses
TrellisCallback blink(keyEvent evt){
  
  if(evt.bit.EDGE == SEESAW_KEYPAD_EDGE_RISING){
    /* trellis.setPixelColor(evt.bit.NUM, 0x090505); //on rising */
    int x = evt.bit.NUM % X_DIM;
    int y = evt.bit.NUM / X_DIM;
    Serial.print(1);
    Serial.print(" ");
    Serial.print(x);
    Serial.print(" ");
    Serial.println(y);
    }else if(evt.bit.EDGE == SEESAW_KEYPAD_EDGE_FALLING){
    /* trellis.setPixelColor(evt.bit.NUM, 0); //off falling */
    int x = evt.bit.NUM % X_DIM;
    int y = evt.bit.NUM / X_DIM;
    Serial.print(0);
    Serial.print(" ");
    Serial.print(x);
    Serial.print(" ");
    Serial.println(y);
    }

  /* trellis.show(); */
  return 0;
}

void setup() {
  Serial.begin(115200);
  //while(!Serial);

  sCmd.addCommand("s", processCommand);
  sCmd.addCommand("c", processCol);
  sCmd.addCommand("off", processOff);
  sCmd.addCommand("r", processRow);
  sCmd.addCommand("l", processList);
  sCmd.addCommand("lc", processListCol);
  /* sCmd.setDefaultHandler(unrecognized); */

  if(!trellis.begin()){
    Serial.println("failed to begin trellis");
    while(1);
  }

  /* the array can be addressed as x,y or with the key number */
  for(int i=0; i<Y_DIM*X_DIM; i++){
      trellis.setPixelColor(i, Wheel(map(i, 0, X_DIM*Y_DIM, 0, 255))); //addressed with keynum
      trellis.show();
      delay(10);
  }
  
  for(int y=0; y<Y_DIM; y++){
    for(int x=0; x<X_DIM; x++){
      //activate rising and falling edges on all keys
      trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_RISING, true);
      trellis.activateKey(x, y, SEESAW_KEYPAD_EDGE_FALLING, true);
      trellis.registerCallback(x, y, blink);
      trellis.setPixelColor(x, y, 0x000000); //addressed with x,y
     trellis.show(); //show all LEDs
      delay(10);
    }
  }

}

void loop() {
  trellis.read();

  while (Serial.available() > 0){
    sCmd.readSerial();
  }
}

void processCommand(){
  char *arg;

  int val;
  int x;
  int y;

  arg = sCmd.next();
  if (arg != NULL){
    val = atoi(arg);
  }

  arg = sCmd.next();
  if (arg != NULL){
    x = atoi(arg);
  }

  arg = sCmd.next();
  if (arg != NULL){
    y = atoi(arg);
  }

  if (val == 0){
    trellis.setPixelColor(x, y, 0x000000); //addressed with x,y
  }else{
    trellis.setPixelColor(x, y, 0x150600); //on rising
  }

  trellis.show();
  
}

void processCol(){
  char *arg;

  uint32_t val;
  int x;
  int y;

  arg = sCmd.next();
  if (arg != NULL){
    uint32_t v;
    sscanf(arg, "%x", &val);
  }

  arg = sCmd.next();
  if (arg != NULL){
    x = atoi(arg);
  }

  arg = sCmd.next();
  if (arg != NULL){
    y = atoi(arg);
  }

  if (val == 0){
    trellis.setPixelColor(x, y, 0x000000); //addressed with x,y
  }else{
    trellis.setPixelColor(x, y, val); //on rising
  }

  trellis.show();
}

void processOff(){
  for(int i=0; i<Y_DIM*X_DIM; i++){
    trellis.setPixelColor(i, 0x000000); 
  }
  trellis.show();
}

void processList(){
  /* Serial.println("ulist1"); */
  char *arg;

  int y = 0;

  arg = sCmd.next();
  if (arg != NULL){
    for(int pos=0; pos<X_DIM*Y_DIM; pos++){
      if (arg[pos] == '0'){
        trellis.setPixelColor(pos, 0x000000);
      }else if (arg[pos] == '1'){
        trellis.setPixelColor(pos, 0x150600);
      }
    }

  }
  trellis.show();
  /* Serial.println("ulist0"); */
}

void processRow(){
  /* Serial.println("ulist1"); */
  char *arg;

  int y = 0;

  arg = sCmd.next();
  if (arg != NULL){
    y = atoi(arg);
  }

  arg = sCmd.next();
  if (arg != NULL){
    for(int x=0; x<X_DIM; x++){
      int pos = X_DIM*y + x;
      if (arg[x] == '0'){
        trellis.setPixelColor(pos, 0x000000);
      }else if (arg[x] == '1'){
        trellis.setPixelColor(pos, 0x150600);
      }
    }

  }
  trellis.show();
  /* Serial.println("ulist0"); */
}

void processListCol(){
  /* Serial.println("ulist1"); */
  char *arg;

  int pos = 0;
  int col = 0;

  arg = sCmd.next();
  while (arg != NULL){
    pos = atoi(arg);

    arg = sCmd.next();
    if (arg != NULL){
      col = atoi(arg);
      trellis.setPixelColor(pos, col);
    }

    arg = sCmd.next();
  }
  trellis.show();
}

void unrecognized(const char $command){
  Serial.println("?");
}
