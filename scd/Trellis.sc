Trellis : NetAddr{
	classvar <>test = 0;
	var num_x = 16;
	var num_y = 8;
	var osc = true;
	var serial;

	var <>state;
	var <>def_col;

	var <>verbose = false;

	*new {
		^super.new;
	}

	init {
		state = Array.fill([num_x, num_y], Color.black);
		def_col = Color.new(0.04, 0.01, 0);
		"init".postln;
	}

	setSerial {
		arg device;
		serial = SerialPort(device, 115200);
		osc = false;
	}


    serial {
        ^serial
    }

	sendSerial {
		arg message;
		message.ascii.do({|i|
			serial.put(i)
		});
		if (verbose, {message.postln});
		serial.put("\n".ascii.at(0));
	}


	s {
		arg x, y, v;
		if (osc, {
			this.sendMsg('/trellis', x, y, v);
		}, {
			this.sendSerial('s '++ v ++' '++ x ++' '++ y);
		});
	}

	on {arg x, y; this.s(x, y, 1);}
	off {arg x, y; this.s(x, y, 0);}

	c {
		arg x, y, col;
		//state[x][y] = col;
		if (verbose, {("col" + x + " " + y + state[x][y]).postln;});
		if (osc, {
			this.sendMsg('/trellis/c', x, y, col.hexString()[1..]);
		},{
			this.sendSerial('c '++ col.hexString()[1..] ++' '++ x ++' '++ y);
		});
	}

	l {
		arg table;
		var arr_out, out;
		arr_out = Array();
		out = "";
		8.do{ arg y;
			16.do{ arg x;
				var pos = y * 16 + x;
				if (table[y][x] == 1, {
					out = out ++ "1";
					arr_out = arr_out.add(pos);
				},{
					out = out ++ "0";
				});
			};
		};

		if (osc, {
			this.sendMsg('/trellis/list', out);
		},{
			this.sendSerial('l '++out);
		});

	}

	all_off {
		if (osc, {
			this.sendMsg('/trellis/off');
		}, {
			this.sendSerial('off');
		});
	}

	emph {
		arg x, y;
		var c_out;
		var c = state[x][y];
		if (c == Color.black, {c_out = def_col;}, {c_out = Color.new(c.red*2, c.green*2, c.blue*2);});
		if (osc, {
			this.on(x, y);
		}, {
			this.sendSerial('c '++ c_out.hexString()[1..] ++' '++ x ++' '++ y);
		});
	}

	deemph {
		arg x, y;
		if (verbose, {("deemph" + x + " " + y + state[x][y]).postln;});
		this.c(x, y, state[x][y]);
	}

	// test {
		// this.all_off();
		// 16.do {arg i;
		// 	SystemClock.sched(i*0.1, {this.on(i, 0);});
		// };
	// }
}
