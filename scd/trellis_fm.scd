s.boot

thisProcess.openUDPPort(9876);

~osc_sender = NetAddr("localhost", 6789);
~osc_sender.sendMsg('/trellis', 0, 2, 1);

OSCFunc.trace(false)

(
SynthDef(\fm, {
	arg freq=500, mRatio=1, cRatio=1, index=1, iScale=5,
	amp=0.2, atk=0.01, rel=3, cAtk=4, cRel=(-4), pan=0;
	var car, mod, env, iEnv;

	iEnv = EnvGen.kr(
		Env.new(
			[index, index * iScale, index],
			[atk, rel],
			[cAtk, cRel]
		)
	);

	env = EnvGen.kr(
		Env.perc(atk, rel, curve: [cAtk, cRel]),
		doneAction:2
	);

	mod = SinOsc.ar(freq * mRatio, mul: freq * mRatio * iEnv);
	car = SinOsc.ar(freq * cRatio + mod) * env * amp;
	car = Pan2.ar(car, pan);
	Out.ar(0, car);
}).add;

~notes = Array.fill(200, nil);
~trellis = OSCFunc({
		arg msg, x, y, val, note;
	x = msg.at(1);
	y = msg.at(2);
	val = msg.at(3);
	note = 40 + x + (5*(3-y));
	note.postln;

	if (val == 1, {
	   ~notes[note] = Synth(\fm, [\freq, note.midicps]);
	},{
		~notes[note].set(\gate, 0);
	});

	}, '/trellis');
)
